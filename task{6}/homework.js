//Task 1

function returnObject(string) {
    let nestedObject = null;
    for (let i = string.length - 1; i >= 0; i--) {
        if (string[i] !== '.') {
            nestedObject = {
                [string[i]]: nestedObject
            }; 
        }
    }
    
    return nestedObject;
}
returnObject('a.b.c.d');

//Task 2

function findEquals(firstArray, secondArray) {
    let thirdArey = secondArray.filter((element) => firstArray.includes(element) === true);
    
    return thirdArey;
}

//Task 3

function mergeArray(firstArray, secondArray) {
    let newArray = firstArray.concat(secondArray);
    let filteredArray = newArray.filter((element, index) => newArray.indexOf(element) == index);
    
    return filteredArray;
}

//Task 4

function mapObject(transform, object) {
    let transformedObject = {};
    for (let key in object) {
        transformedObject[key] = transform(object[key]);
    }

    return transformedObject;
}