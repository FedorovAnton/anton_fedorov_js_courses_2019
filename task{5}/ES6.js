class Person{
    constructor(firstName, lastName, age) {
        if (firstName.search(/^[a-zA-Z]{3,20}\b/)) {
            throw new Error ('the data is incorrect');
        } else {
            this.firstName = firstName;
        }
        if (lastName.search(/^[a-zA-Z]{3,20}\b/)) {
            throw new Error ('the data is incorrect');
        } else {
            this.lastName = lastName;
        }
        if (age <= 0 && age >= 150) {
            throw new Error ('the data is incorrect');
        } else {
            this.age = age;
        }
       
    }
    get fullname() {
        return `${this.firstName} ${this.lastName}`;
    }

    set fullname(newFullName) {
       let names = newFullName.split(' ');
       this.firstName = names[0];
       this.lastName = names[1];
    }

    introduce() {
        return `Hello! My name is ${this.fullName} and I am ${this.age}-years-old`;
    }
}

class Worker extends Person {
    constructor(firstName, lastName, age, experience, salary) {
        super(firstName, lastName, age);
        if (experience >= 1 && experience <= 50) {
            this.experience = experience;
        }
        if (salary >= 1000 && salary <= 10000) {
            this.salary = salary;
        }
    }

    get remunerationRate() {
        let percent;
        if (this.experience >= 1 && this.experience <= 5) {
            percent = 5;
        } else if (this.experience >= 6 &&this.experience <= 10) {
            percent = 10;
        } else if (this.experience >= 11 && this.experience <= 20) {
            percent = 20;
        } else if (this.experience > 20) {
            percent = 50;
        } 
        
        return this.experience * percent / 100;
    }
} 