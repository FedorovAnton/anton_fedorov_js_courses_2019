function Person (firstName, lastName, age) {
    if (firstName.search(/^[a-zA-Z]{3,20}\b/)) {
        throw Error ('the data is incorrect');
    } else {
        this.firstName = firstName;
    }
    if (lastName.search(/^[a-zA-Z]{3,20}\b/)) {
        throw Error ('the data is incorrect');
    } else {
        this.lastName = lastName;
    }
    if (age <= 0 && age >= 150) {
        throw Error ('the data is incorrect');
    } else {
        this.age = age;
    }
}
    Person.prototype = {
        get fullName() {
            return `${this.firstName} ${this.lastName}`;
        },

        set fullName(name) {
            let names = name.split(' ');
            this.firstName = names[0];
            this.lastName = names[1];
        },

        introduce() {
        return `Hello! My name is ${this.fullName} and I am ${this.age}-years-old`;   
        }
    }

        
function Worker (firstName, lastName, age, experience, salary) {
    Person.apply(this, arguments);
    if (experience >= 1 && experience <= 50) {
        this.experience = experience;
    }
    if (salary >= 1000 && salary <= 10000) {
        this.salary = salary;
    }
    
    this.getremunerationRate = function() {
        let percent;
        if (this.experience >= 1 && this.experience <= 5) {
            percent = 5;
        } else if (this.experience >= 6 && this.experience <= 10) {
            percent = 10;
        } else if (this.experience >= 11 && this.experience <= 20) {
            percent = 20;
        } else if (this.experience > 20) {
            percent = 50;
        }

        return this.experience * percent / 100;
    }
    this.remunerationRate = this.getremunerationRate();
} 

Worker.prototype = Object.create(Person.prototype);
Worker.prototype.constructor = Worker;