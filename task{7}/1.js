function debounce(targetFunction, delay) {
    let timerId;
    return function() {
        if (timerId) {
            clearTimeout(timerId);
        }
        timerId = setTimeout(() => targetFunction.apply(this, arguments), delay);
    }
}
