function request(url) {
    return new Promise((res, rej) => {
        const delayTime = Math.floor(Math.random() * 10000) + 1;
        setTimeout(() => res(url), delayTime);
    });
}

function returnResolveUrls(listUrl) {
    let listPromises = listUrl.map((url) => request(url));
    let array = [];

    return new Promise((resolve, reject) => {
        for (let urlPath of listPromises) {
            urlPath
                .then((result) => {
                    array.push(result);
                    resolve(array);
                })
        }
    })
}

 

    


