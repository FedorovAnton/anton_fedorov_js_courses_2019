function makeRandomFn(array) {
	return function() {
		let indexArray = Math.floor(Math.random() * array.length);

		return array[indexArray];
	}
}
