function makeRandomFn(...rangeNumbers) {
	
	return function() {
		let indexArray = Math.floor(Math.random() * rangeNumbers[0].length);
		let indexArguments = Math.floor(Math.random() * rangeNumbers.length);
		
		return indexArray ? rangeNumbers[0][indexArray] : rangeNumbers[indexArguments];
		}
}
 
